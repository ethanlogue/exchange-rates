const axios = require("axios");
const cheerio = require("cheerio");
const express = require("express");
const router = express.Router();

let dumbError = {
  error: {
    just: {
      refresh: "please",
    },
  },
};
let result = {
  EU: {
    rate: "",
    date: "",
  },
  UK: {
    rate: "",
    date: "",
  },
  CH: {
    rate: "",
    date: "",
  },
};

const cleanResponse = (response) => {
  let setTool = cheerio.load(response);
  //european union
  let resEU = setTool(
    `html body table table:nth-of-type(2) table tr:nth-of-type(2) td.kurslisteTitleVrb:nth-of-type(4)`
  );
  let resEUDate = setTool(
    `html body table table:nth-of-type(2) table tr:nth-of-type(2) td.kurslisteTitleVrb:nth-of-type(6)`
  );
  //united kingdom
  let resUK = setTool(
    `html body table table:nth-of-type(2) table tr:nth-of-type(3) td.kurslisteTitleVrb:nth-of-type(4)`
  );
  let resUKDate = setTool(
    `html body table table:nth-of-type(2) table tr:nth-of-type(3) td.kurslisteTitleVrb:nth-of-type(6)`
  );
  //switzerland
  let resCH = setTool(
    `html body table table:nth-of-type(2) table tr:nth-of-type(5) td.kurslisteTitleVrb:nth-of-type(4)`
  );
  let resCHDate = setTool(
    `html body table table:nth-of-type(2) table tr:nth-of-type(5) td.kurslisteTitleVrb:nth-of-type(6)`
  );

  //maths
  result.EU.rate = cleanResultNumber("EU", resEU.text());
  result.UK.rate = cleanResultNumber("UK", resUK.text());
  result.CH.rate = cleanResultNumber("CH", resCH.text());

  //dates
  result.EU.date = resEUDate.text();
  result.UK.date = resUKDate.text();
  result.CH.date = resCHDate.text();
};

const cleanResultNumber = (country, rate) => {
  let numberVal = parseFloat(rate.replace(/\s/g, "").replace(",", "."));
  numberVal = numberVal.toString();
  numberVal = Number(numberVal.slice(0, 6));

  if (country === "EU") {
    numberVal = 1 / (numberVal + 0.003);
    numberVal = +numberVal.toFixed(4);
    return numberVal;
  }
  if (country !== "EU" && result.EU.rate) {
    numberVal = result.EU.rate * (numberVal - 0.002);
    numberVal = +numberVal.toFixed(4);
    return numberVal;
  }

  return numberVal;
};

const requestRate = () => {
  axios
    .get("https://bvapps.vr-bankenportal.de/gla/devisenkurse/D810.html")
    .then((response) => {
      response && response.data && cleanResponse(response.data);
    })
    .catch((error) => {
      console.log(error);
    });
};

const resultResponse = () => {
  requestRate();
  if (result.EU.date.length > 0) {
    return result;
  }

  return dumbError;
};

router.use((req, res, next) => {
  next();
});

router.get("/", (req, res) => {
  res.send(resultResponse());
});

module.exports = router;
