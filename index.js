const express = require("express");

const calc = require("./calc");

const app = express();
const port = 8080;

let html = `
    <!DOCTYPE html>

    <html lang="en">

    <head>

    <title>Rate Conversion Tool</title>

    <meta charset="UTF-8" />

    <meta name="viewport" content="width=device-width,initial-scale=1" />

    <meta name="description" content="oddly specific, janky rate conversion tool" />

    </head>

    <body>

        <p>Yeah idk, you probably meant to load <a href="/calc">/calc</a> to use my janky rate tool don't you?</p>

    </body>
    </html>
`;

app.use(express.static("public"));

app.use("/calc", calc);

app.get("/", (req, res) => {
  res.send(html);
});

app.listen(port, () => {});
